<?php

if ( empty($_POST['id']) ) {
	//Faz consulta preparada para evitar SQL injection
	$preparada = mysqli_prepare( $db, '	INSERT INTO tb_bitbucket
										( nome, url, ip)
										VALUES
										( ?, ?, ?)');

	mysqli_stmt_bind_param(	$preparada, 
							'sss', 
							$nome, 
							$url, 
                            $_SERVER['REMOTE_ADDR']);
                            
	echo mysqli_stmt_error($preparada);

	if( mysqli_stmt_execute($preparada)){

		echo "<br><br>Dados de $nome gravados no SGDB!";
	}
	//FIM Faz consulta preparada para evitar SQL injection

} elseif ( is_numeric($_POST['id']) ) {

    $preparada = mysqli_prepare( $db, "UPDATE tb_bitbucket SET nome=?, url=?, ip=? WHERE id=? ");
    mysqli_stmt_bind_param($preparada, 'ssii', $nome, $url, $_SERVER['REMOTE_ADDR'], $_POST['id']);
    mysqli_stmt_execute($preparada);
}

<?php

//Conecta no banco
require 'conn.php';

$dados['nome'] = $dados['url'] = $id = null;

//Botão Editar
include 'editar.php';

// Formulário 
echo "	<form method='post'>
			<input 	type='hidden'
					name='id'
					value='$id'>
			Nome: <input type='text' 
						 name='nome'
						 value='{$dados['nome']}'>
						 <br>
			Bitbucket: <input type='text'
							  name='url'
							 value='{$dados['url']}'>
								<br>
			<input  type='submit' 
					value='salvar'>
		</form>";

//Dados inseridos
$nome = isset($_POST['nome']) ? $_POST['nome'] : null;		

$url = isset($_POST['url']) ? $_POST['url'] : null;

// Registro no banco com os dados do usuário
include 'registro.php';

// Deletar Dado do banco
include 'deletar.php';

//Preenchendo tela com dados do usuário
$objConsulta = mysqli_query( $db, '	SELECT
										id, 
										nome,
										url,
										ip
									FROM 
										tb_bitbucket ');

echo "<form method='post'>";
echo "<table>
		<tr>
			<td>ID</td>
			<td>Nome</td>
			<td>URL</td>
			<td>IP</td>
			<td>&nbsp;</td>
		</tr>";

while($reg = $objConsulta->fetch_assoc()){

	echo "	<tr>
				<td>{$reg['id']}</td>
				<td>{$reg['nome']}</td>
				<td>{$reg['url']}</td>
				<td>{$reg['ip']}</td>
				<td>
					<button name='editar'
							value='{$reg['id']}'>
						Editar
					</button>

				</td>
				<td>
				<button name='deletar'
					value='{$reg['id']}'>
					Deletar
				</button>
				</td>
			</tr>";
}

echo "</table>";
echo "</form>";

exit();
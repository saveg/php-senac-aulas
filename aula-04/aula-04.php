<?php

echo '<pre>';

// var_dump($_SERVER);

echo "Seu IP é: " . $_SERVER['REMOTE_ADDR'];

// echo "Seu IP é: {$_SERVER['REMOTE_ADDR']}";

echo '<br> <hr> <br>';

// phpinfo();

echo '<br> <hr> <br>';

// Vetor com mais de uma dimensão

$alunos[0]['nome'] = 'Vinicius Savegnago';
$alunos[0]['Bitbucket'] = 'https://bitbucket.org/saveg/';

$carros[0]['ano'] = '1969';
$carros[0]['nome'] = 'Vouquisvaguen';

/* OU -> */ 
$alunas = array(0 => array('nome' => 'Itchabela',
'bitbucket' => 'isento'
));

$objetos = array(0 => array(
'nome' => 'Caneta',
    'preço' => 'R$799.99'
));




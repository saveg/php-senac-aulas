<?php

class Usuario{

    
    private $id;
    private $email;
    private $senha;
    private $objDb;
    public function __construct(){

        $this->objDb = new mysqli('localhost', 'root', '', 'aula_php', 3307);

    }
    
    //Setters
    public function setId(int $id){

        $this->id = $id;

    }

    public function setEmail(string $email){

        $this->email = $email;

    }

    public function setSenha(string $senha){

        $this->senha = $hash = password_hash( $this->senha, PASSWORD_DEFAULT);

    }

    public function vanish(){

        $objStmt = $this->objDb->prepare('DELETE FROM users WHERE id = ?');
        
        $objStmt->bind_param('i', $this->id);

        if($objStmt->execute()){
            return true;
        }else{
            return false;
        }
                                    
    }

    public function listarUsuario(){
        $objListar = $this->objDb->query("SELECT 
                                            id, email, password 
                                            FROM 
                                            users
                                          WHERE id = {$this->id}");

        return $objListar;
    }


    // Getters
    public function getId(int $id) : int{

        return $this->id;

    }

    public function getEmail(string $email) : string{

        return $this->email;

    }

    public function getSenha(string $senha) : string{

        return $this->senha;

    }

    public function saveUsuario(){

        $objStmt = $this->objDb->prepare('REPLACE INTO users
        ( id, email, password)
        VALUES
        ( ?, ?, ?)');
        
        
        $objStmt->bind_param('iss', $this->id,
                                    $this->email,
                                    $this->senha);

        if($objStmt->execute()){
            return true;
        }else{
            return false;
        }
                                    
    }

    public function __destruct(){
        unset($this->objDb);
    }
}